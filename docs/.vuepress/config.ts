import { defineUserConfig, defaultTheme } from "vuepress";
import { searchPlugin } from "@vuepress/plugin-search";
import { navbar } from "./routes/navbar";
import { sidebar } from "./routes/sidebar";

export default defineUserConfig({
  base: process.env.NODE_ENV === "development" ? "/" : "/",
  lang: "zh-CN",
  title: "算法笔记",
  description: "算法笔记",
  head: [
    ["link", { rel: "icon", href: "/images/logo.jpg" }],
    ['script', { type: 'text/javascript', src: 'https://readmore.openwrite.cn/js/readmore.js' }],
    ['script', { type: 'text/javascript', src: '/js/openwrite.js' }],
  ],
  plugins: [
    searchPlugin({
      // 配置项
      locales: {
        "/": {
          placeholder: "输入s进行搜索",
        },
      },
      maxSuggestions: 10,
      isSearchable: (page) => page.path !== "/",
    }),
    // backToTopPlugin(),
  ],
  theme: defaultTheme({
    logo: "/images/logo.jpg",
    logoDark: "/images/logoDark.png",
    repo: "https://gitee.com/songsshao_admin/algorithm-note.git",
    repoLabel: "点亮⭐不迷路",
    docsDir: "docs",
    editLinkText: "为该章节纠错",
    lastUpdatedText: "上次更新",
    navbar: navbar,
    sidebar: sidebar,
    sidebarDepth: 2,
  }),
});
