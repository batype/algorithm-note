export const navbar = [
  // 嵌套 Group - 最大深度为 2
  {
    text: "牛客",
    children: [
      {
        text: "华为",
        link: "/pages/niuke-hj/01.字符串最后一个单词的长度.md",
      },
    ],
  },
  // 控制元素何时被激活
  {
    text: "力扣",
    children: [
      {
        text: "数组",
        link: "/pages/leetcode/array/00.数组理论基础.md",
      },
      {
        text: "链表",
        link: "/pages/leetcode/ListNode/00.链表理论基础.md",
      },
      {
        text: "哈希表",
        link: "/pages/leetcode/HasTable/00.哈希表.md",
      },
      {
        text: "字符串",
        link: "/pages/leetcode/String/01.字符串反转.md",
      },
    ],
  },
  {
    text: "练习算法",
    link: "/pages/leetcode/practice/3.无重复字符的最长子串.md",
  },
];
