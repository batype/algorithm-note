import { leetcode } from "../routes/leetcode";

export const sidebar = [
  {
    text: "牛客网",
    collapsible: true,
    children: [
      // SidebarItem
      {
        text: "华为算法",
        collapsible: true,
        children: [
          {
            text: "1.字符串最后一个单词的长度",
            link: "/pages/niuke-hj/01.字符串最后一个单词的长度.md",
          },
          {
            text: "2.计算某字符出现次数",
            link: "/pages/niuke-hj/02.计算某字符出现次数.md",
          },
          {
            text: "3.明明的随机数",
            link: "/pages/niuke-hj/03.明明的随机数.md",
          },
          {
            text: "4.字符串分隔",
            link: "/pages/niuke-hj/04.字符串分隔.md",
          },
          { text: "5.进制转换", link: "/pages/niuke-hj/05.进制转换.md" },
          { text: "6.质数因子", link: "/pages/niuke-hj/06.质数因子.md" },
          { text: "7.取近似值", link: "/pages/niuke-hj/07.取近似值.md" },
          {
            text: "8.合并表记录",
            link: "/pages/niuke-hj/08.合并表记录.md",
          },
          {
            text: "9.提取不重复的整数",
            link: "/pages/niuke-hj/09.提取不重复的整数.md",
          },
          {
            text: "10.字符个数统计",
            link: "/pages/niuke-hj/10.字符个数统计.md",
          },
          { text: "11.数字颠倒", link: "/pages/niuke-hj/11.数字颠倒.md" },
          {
            text: "12.字符串反转",
            link: "/pages/niuke-hj/12.字符串反转.md",
          },
          { text: "13.句子逆序", link: "/pages/niuke-hj/13.句子逆序.md" },
          {
            text: "14.字符串排序",
            link: "/pages/niuke-hj/14.字符串排序.md",
          },
          {
            text: "15.求int型正整数在内存中存储时1的个数",
            link: "/pages/niuke-hj/15.求int型正整数在内存中存储时1的个数.md",
          },
          { text: "16.购物单", link: "/pages/niuke-hj/16.购物单.md" },
          { text: "17.坐标移动", link: "/pages/niuke-hj/17.坐标移动.md" },
          {
            text: "18.识别有效的IP地址和掩码并进行分类统计",
            link: "/pages/niuke-hj/18.识别有效的IP地址和掩码并进行分类统计.md",
          },
          {
            text: "19.简单错误记录",
            link: "/pages/niuke-hj/19.简单错误记录.md",
          },
          {
            text: "20.密码验证合格程序",
            link: "/pages/niuke-hj/20.密码验证合格程序.md",
          },

          { text: "21.简单密码", link: "/pages/niuke-hj/21.简单密码.md" },

          { text: "22.汽水瓶", link: "/pages/niuke-hj/22.汽水瓶.md" },
          {
            text: "23.删除字符串中出现次数最少的字符",
            link: "/pages/niuke-hj/23.删除字符串中出现次数最少的字符.md",
          },

          { text: "24.合唱队", link: "/pages/niuke-hj/24.合唱队.md" },

          {
            text: "25.数据分类处理",
            link: "/pages/niuke-hj/25.数据分类处理.md",
          },
          {
            text: "26.字符串排序",
            link: "/pages/niuke-hj/26.字符串排序.md",
          },
          {
            text: "27.查找兄弟单词",
            link: "/pages/niuke-hj/27.查找兄弟单词.md",
          },
          { text: "28.素数伴侣", link: "/pages/niuke-hj/28.素数伴侣.md" },
          {
            text: "29.字符串加解密",
            link: "/pages/niuke-hj/29.字符串加解密.md",
          },
          {
            text: "30.字符串合并处理",
            link: "/pages/niuke-hj/30.字符串合并处理.md",
          },
          { text: "31.单词倒排", link: "/pages/niuke-hj/31.单词倒排.md" },
          { text: "32.密码截取", link: "/pages/niuke-hj/32.密码截取.md" },
          {
            text: "33.整数与IP地址间的转换",
            link: "/pages/niuke-hj/33.整数与IP地址间的转换.md",
          },
          { text: "34.图片整理", link: "/pages/niuke-hj/34.图片整理.md" },
          { text: "35.蛇形矩阵", link: "/pages/niuke-hj/35.蛇形矩阵.md" },
          {
            text: "36.字符串加密",
            link: "/pages/niuke-hj/36.字符串加密.md",
          },
          {
            text: "37.统计每个月兔子的总数",
            link: "/pages/niuke-hj/37.统计每个月兔子的总数.md",
          },
          {
            text: "38.求小球落地5次后所经历的路程和第5次反弹的高度",
            link: "/pages/niuke-hj/38.求小球落地5次后所经历的路程和第5次反弹的高度.md",
          },
          {
            text: "39.判断两个IP是否属于同一子网",
            link: "/pages/niuke-hj/39.判断两个IP是否属于同一子网.md",
          },
          { text: "40.统计字符", link: "/pages/niuke-hj/40.统计字符.md" },
          { text: "41.称砝码", link: "/pages/niuke-hj/41.称砝码.md" },
          { text: "42.学英语", link: "/pages/niuke-hj/42.学英语.md" },
          { text: "43.迷宫问题", link: "/pages/niuke-hj/43.迷宫问题.md" },
          { text: "44.Sudoku", link: "/pages/niuke-hj/44.Sudoku.md" },
          {
            text: "45.名字的漂亮度",
            link: "/pages/niuke-hj/45.名字的漂亮度.md",
          },
          {
            text: "46.截取字符串",
            link: "/pages/niuke-hj/46.截取字符串.md",
          },
          {
            text: "48.从单向链表中删除指定值的节点",
            link: "/pages/niuke-hj/48.从单向链表中删除指定值的节点.md",
          },
          { text: "50.四则运算", link: "/pages/niuke-hj/50.四则运算.md" },
          {
            text: "51.输出单向链表中倒数第k个结点",
            link: "/pages/niuke-hj/51.输出单向链表中倒数第k个结点.md",
          },
          {
            text: "53.杨辉三角的变形",
            link: "/pages/niuke-hj/53.杨辉三角的变形.md",
          },
          {
            text: "54.表达式求值",
            link: "/pages/niuke-hj/54.表达式求值.md",
          },
          {
            text: "56.完全数计算",
            link: "/pages/niuke-hj/56.完全数计算.md",
          },
          {
            text: "58.输入n个整数，输出其中最小的k个",
            link: "/pages/niuke-hj/58.输入n个整数，输出其中最小的k个.md",
          },
          {
            text: "60.查找组成一个偶数最接近的两个素数",
            link: "/pages/niuke-hj/60.查找组成一个偶数最接近的两个素数.md",
          },
          { text: "61.放苹果", link: "/pages/niuke-hj/61.放苹果.md" },
          {
            text: "62.查找输入整数二进制中1的个数",
            link: "/pages/niuke-hj/62.查找输入整数二进制中1的个数.md",
          },
          {
            text: "63.百钱买百鸡问题",
            link: "/pages/niuke-hj/72.百钱买百鸡问题.md",
          },
          {
            text: "64.求解连续数列",
            link: "/pages/niuke-hj/201.求解连续数列.md",
          },
          {
            text: "65.查找众数及中位数",
            link: "/pages/niuke-hj/202.查找众数及中位数.md",
          },
          {
            text: "66.找相同子串",
            link: "/pages/niuke-hj/203.找相同子串.md",
          },
          {
            text: "67.字符串统计",
            link: "/pages/niuke-hj/204.字符串统计.md",
          },
          {
            text: "68.磁盘容量排序",
            link: "/pages/niuke-hj/205.磁盘容量排序.md",
          },
          {
            text: "69.太阳能板最大面积",
            link: "/pages/niuke-hj/206.太阳能板最大面积.md",
          },
          {
            text: "70.出租车计费",
            link: "/pages/niuke-hj/207.出租车计费.md",
          },
          {
            text: "71.整数对最小和",
            link: "/pages/niuke-hj/208.整数对最小和.md",
          },
          {
            text: "72.判断字符串子序列",
            link: "/pages/niuke-hj/209.判断字符串子序列.md",
          },
          {
            text: "73.运动会排队",
            link: "/pages/niuke-hj/210.运动会排队.md",
          },
          { text: "74.快递运输", link: "/pages/niuke-hj/211.快递运输.md" },
          { text: "75.TLV解码", link: "/pages/niuke-hj/212.TLV解码.md" },
          { text: "76.考勤信息", link: "/pages/niuke-hj/213.考勤信息.md" },
          {
            text: "77.字符串分割",
            link: "/pages/niuke-hj/214.字符串分割.md",
          },
          {
            text: "78.组成最大数",
            link: "/pages/niuke-hj/215.组成最大数.md",
          },
          {
            text: "79.高矮个子排队",
            link: "/pages/niuke-hj/216.高矮个子排队.md",
          },
          {
            text: "80.简易线程池",
            link: "/pages/niuke-hj/230.简易线程池.md",
          },
          {
            text: "81.发现新词的数量-新词挖掘",
            link: "/pages/niuke-hj/240.发现新词的数量-新词挖掘.md",
          },
          {
            text: "82.信号发射和接收",
            link: "/pages/niuke-hj/信号发射和接收.md",
          },
        ],
      },
    ],
  },
  leetcode,
  {
    text: "练习算法",
    collapsible: true,
    children: [
      {
        text: "1.无重复字符的最长子串",
        link: "/pages/leetcode/practice/3.无重复字符的最长子串.md",
      },
      {
        text: "2.最大子数组和(贪心算法)",
        link: "/pages/leetcode/practice/53.最大子序和.md",
      },
      {
        text: "3.跳跃游戏",
        link: "/pages/leetcode/practice/45.跳跃游戏II.md",
      },
      {
        text: "4.跳跃游戏(贪心算法)",
        link: "/pages/leetcode/practice/55.跳跃游戏.md",
      },
      {
        text: "5.排列序列",
        link: "/pages/leetcode/practice/60.排列序列.md",
      },
      {
        text: "6.不同路径",
        link: "/pages/leetcode/practice/62.不同路径.md",
      },
      {
        text: "7.买卖股票的最佳时机II(贪心算法)",
        link: "/pages/leetcode/practice/122.买卖股票的最佳时机II.md",
      },
      {
        text: "8.单词接龙",
        link: "/pages/leetcode/practice/127.单词接龙.md",
      },
      {
        text: "9.最大数",
        link: "/pages/leetcode/practice/179.最大数.md",
      },
      {
        text: "10.摆动序列(贪心算法)",
        link: "/pages/leetcode/practice/376.摆动序列.md",
      },
      {
        text: "11.判断子序列",
        link: "/pages/leetcode/practice/392.判断子序列.md",
      },
      {
        text: "12.分发饼干(贪心算法)",
        link: "/pages/leetcode/practice/455.分发饼干.md",
      },
      {
        text: "13.删除字符串中的所有相邻重复项",
        link: "/pages/leetcode/practice/1047.删除字符串中的所有相邻重复项.md",
      },
      {
        text: "14.(暂未解出)最多K次交换相邻数位后得到的最小整数",
        link: "/pages/leetcode/practice/1505.(暂未解出)最多K次交换相邻数位后得到的最小整数.md",
      },
      {
        text: "15.拆分字符串使唯一子字符串的数目最大",
        link: "/pages/leetcode/practice/1593.拆分字符串使唯一子字符串的数目最大.md",
      },
      {
        text: "16.得到连续K个1的最少相邻交换次数",
        link: "/pages/leetcode/practice/1703.得到连续K个1的最少相邻交换次数.md",
      },
    ],
  },
];
