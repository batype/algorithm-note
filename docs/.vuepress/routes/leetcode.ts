import generateArrayFromDirectory from "./generateArrayFromDirectory";

const array = [
  {
    text: "01.数组理论基础",
    link: "/pages/leetcode/array/00.数组理论基础.md",
  },
  {
    text: "02.二分查找",
    link: "/pages/leetcode/array/01.二分查找.md",
  },
  {
    text: "03.移除元素",
    link: "/pages/leetcode/array/02.移除元素.md",
  },
  {
    text: "04.有序数组的平方",
    link: "/pages/leetcode/array/03.有序数组的平方.md",
  },
  {
    text: "05.长度最小的子数组",
    link: "/pages/leetcode/array/04.01.长度最小的子数组.md",
  },
  {
    text: "06.水果成蓝",
    link: "/pages/leetcode/array/04.02.水果成蓝.md",
  },
  {
    text: "07.最小覆盖子串",
    link: "/pages/leetcode/array/04.03.最小覆盖子串.md",
  },
  {
    text: "08.螺旋矩阵II",
    link: "/pages/leetcode/array/05.02.螺旋矩阵II.md",
  },
];

const LinkNode = [
  {
    text: "01.链表理论基础",
    link: "/pages/leetcode/ListNode/00.链表理论基础.md",
  },
  {
    text: "02.移除链表元素",
    link: "/pages/leetcode/ListNode/01.移除链表元素.md",
  },
  {
    text: "03.设计链表",
    link: "/pages/leetcode/ListNode/02.设计链表.md",
  },
  {
    text: "04.反转链表",
    link: "/pages/leetcode/ListNode/03.反转链表.md",
  },
  {
    text: "05.两两交换链表中的节点",
    link: "/pages/leetcode/ListNode/04.两两交换链表中的节点.md",
  },
  {
    text: "06.删除链表的倒数第N个节点",
    link: "/pages/leetcode/ListNode/05.删除链表的倒数第N个节点.md",
  },
  {
    text: "07.链表相交",
    link: "/pages/leetcode/ListNode/06.链表相交.md",
  },
  {
    text: "08.环形链表II",
    link: "/pages/leetcode/ListNode/07.环形链表II.md",
  },
];

const hasTable = [
  {
    text: "01.哈希表理论基础",
    link: "/pages/leetcode/HasTable/00.哈希表.md",
  },
  {
    text: "02.有效的字母异位词",
    link: "/pages/leetcode/HasTable/01.有效的字母异位词.md",
  },
  {
    text: "03.两个数组的交集",
    link: "/pages/leetcode/HasTable/02.两个数组的交集.md",
  },
  {
    text: "04.快乐数",
    link: "/pages/leetcode/HasTable03.快乐数.md",
  },
  {
    text: "05.两数之和",
    link: "/pages/leetcode/HasTable/04.两数之和.md",
  },
  {
    text: "06.赎金信",
    link: "/pages/leetcode/HasTable/06.赎金信.md",
  },
  {
    text: "07.三数之和",
    link: "/pages/leetcode/HasTable/07.三数之和.md",
  },
  {
    text: "08.四数之和",
    link: "/pages/leetcode/HasTable/08.四数之和.md",
  },
];

const string = [
  {
    text: "01.字符串反转",
    link: "/pages/leetcode/String/01.字符串反转.md",
  },
  {
    text: "02.反转字符串II",
    link: "/pages/leetcode/String/02.反转字符串II.md",
  },
  {
    text: "03.替换空格",
    link: "/pages/leetcode/String/03.替换空格.md",
  },
  {
    text: "04.反转字符串中的单词",
    link: "/pages/leetcode/String/04.反转字符串中的单词.md",
  },
  {
    text: "05.左旋转字符串",
    link: "/pages/leetcode/String/05.左旋转字符串.md",
  },
  {
    text: "06.实现strStr",
    link: "/pages/leetcode/String/06.实现strStr.md",
  },
  {
    text: "07.重复的子字符串",
    link: "/pages/leetcode/String/07.重复的子字符串.md",
  },
];

const double = [
  {
    text: "01.移除元素",
    link: "/pages/leetcode/DoublePointer/01.移除元素.md",
  },
  {
    text: "02.反转字符串",
    link: "/pages/leetcode/String/01.字符串反转.md",
  },
  {
    text: "03.替换数字",
    link: "/pages/leetcode/DoublePointer/03.替换数字.md",
  },
  {
    text: "04.反转字符串中的单词",
    link: "/pages/leetcode/String/04.反转字符串中的单词.md",
  },
  {
    text: "05.反转链表",
    link: "/pages/leetcode/ListNode/03.反转链表.md",
  },
  {
    text: "06.删除链表的倒数第N个节点",
    link: "/pages/leetcode/ListNode/05.删除链表的倒数第N个节点.md",
  },
  {
    text: "07.链表相交",
    link: "/pages/leetcode/ListNode/06.链表相交.md",
  },
  {
    text: "08.环形链表II",
    link: "/pages/leetcode/ListNode/07.环形链表II.md",
  },
  {
    text: "09.三数之和",
    link: "/pages/leetcode/HasTable/07.三数之和.md",
  },
  {
    text: "10.四数之和",
    link: "/pages/leetcode/HasTable/08.四数之和.md",
  },
]

const StackQueue = [
  {
    text: '01.栈与队列理论基础',
    link: '/pages/leetcode/StackQueue/01.栈与队列理论基础.md'
  },
  {
    text: '02.用栈实现队列',
    link: '/pages/leetcode/StackQueue/02.用栈实现队列.md'
  },
  {
    text: '03.用队列实现栈',
    link: '/pages/leetcode/StackQueue/03.用队列实现栈.md'
  },
  { text: '04.有效的括号', link: '/pages/leetcode/StackQueue/04.有效的括号.md' },
  {
    text: '05.删除字符串中的所有相邻重复项',
    link: '/pages/leetcode/StackQueue/05.删除字符串中的所有相邻重复项.md'
  },
  {
    text: '06.逆波兰表达式求值',
    link: '/pages/leetcode/StackQueue/06.逆波兰表达式求值.md'
  },
  {
    text: '07.滑动窗口最大值',
    link: '/pages/leetcode/StackQueue/07.滑动窗口最大值.md'
  },
  {
    text: '08.前 K 个高频元素',
    link: '/pages/leetcode/StackQueue/08.前K个高频元素.md'
  }
]

export const leetcode = {
  text: "力扣",
  collapsible: true,
  children: [
    // SidebarItem
    {
      text: "数组",
      collapsible: true,
      children: array,
    },
    {
      text: "链表",
      collapsible: true,
      children: LinkNode,
    },
    {
      text: "哈希表",
      collapsible: true,
      children: hasTable,
    },
    {
      text: "字符串",
      collapsible: true,
      children: string,
    },
    {
      text: "双指针",
      collapsible: true,
      children: double,
    },
    {
      text: '栈与队列',
      collapsible: true,
      children: StackQueue
    }
  ],
};
