const fs = require('fs');

function generateArrayFromDirectory(directoryPath) {
  const files = fs.readdirSync(__dirname + '\\..\\..' + directoryPath);
  const array = [];

  files.forEach(file => {
    if(file.endsWith('.md')) {
        const fileName = file.replace('.md', '');
        const filePath = `${directoryPath}/${file}`;
        // const link = filePath.replace('path/to/root', '');
    
        array.push({
          text: fileName,
          link: filePath
        });
    }
    
  });

  return array;
}

const StackQueue = generateArrayFromDirectory('/pages/leetcode/StackQueue');
console.log(StackQueue)