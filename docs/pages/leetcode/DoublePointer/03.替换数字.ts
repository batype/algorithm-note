const STR0 = '0'.charCodeAt(0);
const STR9 = '9'.charCodeAt(0);

const STRA = 'a'.charCodeAt(0);
const STRZ = 'z'.charCodeAt(0);

function isStr(str): boolean {
    if(str >= STRA && str <= STRZ) {
        return true;
    }
    return false;
}

function isNum(str): boolean {
    if(str >= STR0 && str <= STR9) {
        return true;
    }
    return false;
}

// number
function replaceNumberToString(str: string): string {
    let len = 0;
    for(let i = 0; i < str.length; i++) {
        if(isNum(str[i])) {
            len += 6;
        }
        if(isStr(str[i])) {
            len++;
        }
    }
    let arr = new Array(len).fill('');
    let index = str.length - 1;
    for(let i = len - 1; i >= 0; i--) {
        if(isNum(str[index])) {
            arr[i] = 'r';
            arr[i - 1] = 'e';
            arr[i - 2] = 'b';
            arr[i - 3] = 'm';
            arr[i - 4] = 'u';
            arr[i - 5] = 'n';
            i -= 5;
        }
        if(isStr(str[index])) {
            arr[i] = str[index];
        }
        index--;
    }
    return arr.join('');
}

console.log(replaceNumberToString('a5b'));