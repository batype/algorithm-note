function removeElement(nums: number[], val: number): number {
    let slowIndex: number = 0;
    for(let fastIndex = 0; fastIndex < nums.length; fastIndex++) {
        if(nums[fastIndex] !== val) {
            nums[slowIndex++] = nums[fastIndex];
        }
    }
    return slowIndex;
};