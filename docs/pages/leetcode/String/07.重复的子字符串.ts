function getNext(s: string): number[] {
    let j = 0;
    let next:number[] = [];
    next[0] = 0;
    for(let i = 1; i < s.length; i++) {
        while(j > 0 && s[i] !== s[j]) {
            j = next[j - 1];
        }
        if(s[i] === s[j]) j++;
        next[i] = j;
    }
    return next;
}

function repeatedSubstringPattern(s: string): boolean {
    let next = getNext(s);
    let len = s.length;
    return next[len - 1] > 0 && len % (len - next[len - 1]) === 0;
};