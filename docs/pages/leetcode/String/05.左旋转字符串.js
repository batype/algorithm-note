function reverseLeftWords(str, n) {
    function reverseWord(strArr, start, end) {
        let temp;
        while(start < end) {
            temp = strArr[start];
            strArr[start] = strArr[end];
            strArr[end] = temp;
            start++;
            end--;
        }
    }

    let strArr = str.split('');
    let length = strArr.length;
    if(length < n) return str;
    reverseWord(strArr, 0, n - 1);
    reverseWord(strArr, n, length - 1);
    reverseWord(strArr, 0, length - 1);
    return strArr.join('');
}

console.log(reverseLeftWords('abcdefg', 2));
console.log(reverseLeftWords('abcdefg', 99));
