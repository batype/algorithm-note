function getNext(next, s) {
    // j 表示前缀字符串的长度
    let j = 0;
    next[0] = 0;
    for(let i = 1; i < s.length; i++) {
        // 前后缀不相等，则前缀字符串长度为 next[j - 1]
        while(j > 0 && s[i] !== s[j]) {
            j = next[j - 1];
        }
        // 前后缀相等，则前缀字符串长度为 next[j]
        if(s[i] === s[j]){
            j++;
        }
        // 更新 next[i]
        next[i] = j;
    }
}

function strStr(haystack, needle) {
    if(needle.length === 0) {
        return 0;
    }
    let next = [];
    getNext(next, needle);
    let j = 0;
    for(let i = 0; i < haystack.length; i++) {
        // 如果文本串和模式串不相等，则j 需要取 next[j - 1]等于j;
        while(j > 0 && haystack[i] !== needle[j]) {
            j = next[j - 1];
        }
        // 如果文本串和模式串相等，j++
        if(haystack[i] === needle[j]) {
            j++;
        }
        // 如果j === needle.length，说明匹配到了
        // 返回匹配到的位置
        if(j === needle.length) {
            return i - needle.length + 1;
        }
    }
    return -1;
}
let haystack = 'aabaabaaf';
let needle   = 'aabaaf';
console.log(strStr(haystack, needle));