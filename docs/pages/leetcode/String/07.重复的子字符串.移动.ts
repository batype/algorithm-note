/**
 * 结题思路，是将两个s + s,组成新的字符串，然后去除第一个和最后一个节点，原因是不能让它自己和自己匹配。
 * 如果t包含s, 则有重复放入子字符串。
 * @param s 
 * @returns 
 */
function repeatedSubstringPattern(s: string): boolean {
    let t = s + s;
    t = t.slice(1, -1);
    return t.includes(s);
};