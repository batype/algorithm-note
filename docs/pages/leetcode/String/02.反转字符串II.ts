function reverseStr(s: string, k: number): string {
  let left: number, right: number;
  let strArr = s.split("");
  let length: number = strArr.length;
  let temp: string;
  for (let i = 0; i < length; i += 2 * k) {
    left = i;
    right = i + k - 1 >= length ? length - 1 : i + k - 1;
    while (left < right) {
      temp = strArr[left];
      strArr[left] = strArr[right];
      strArr[right] = temp;
      left++;
      right--;
    }
  }

  return strArr.join("");
}

console.log(reverseStr("abcdefg", 2));
