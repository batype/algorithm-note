function reverseLeftWords(str: string, n: number): string {

    function reverseWords(strArr: string[], start: number, end: number): void {
        let temp;
        while(start < end) {
            temp = strArr[start];
            strArr[start] = strArr[end];
            strArr[end] = temp;
            start++;
            end--;
        }
    }

    let strArr: string[] = str.split('');
    let length: number = strArr.length - 1;
    reverseWords(strArr, 0, n);
    reverseWords(strArr, n+1, length);
    reverseWords(strArr, 0, length);
    return strArr.join('');
}

console.log(reverseLeftWords('abcdefg', 2));
console.log(123);
