function strStr(haystack: string, needle: string): number {
    if(needle.length === 0) return 0;
    let j: number = 0;
    let next: number[] = getNext(needle);
    for(let i = 1; i < haystack.length; i++) {
        while(j > 0 && haystack[i] !== needle[j]) {
            j = next[j -1];
        }
        if(haystack[i] === needle[j]) j++;
        if(j === needle.length){
            return i - needle.length + 1;
        }
    }
    return -1;
}

function getNext(str: string): number[] {
    let next: number[] = [];
    let j: number = 0;
    next[0] = 0;

    for(let i = 1; i < str.length; i++) {
        while(j > 0 && str[i] !== str[j]) {
            j = next[j - 1];
        }
        if(str[i] === str[j]) {
            j++;
        }
        next[i] = j;
    }
    return next;
}

console.log(strStr("aabaaabaaac", "aabaaac"));