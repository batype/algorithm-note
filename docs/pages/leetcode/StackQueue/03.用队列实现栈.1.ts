class MyStack {
    private queue: number[];
    constructor() {
        this.queue = [];
    }

    push(x: number): void {
        this.queue.push(x);
    }

    pop(): number {
       let size:number = this.queue.length;
       while(size-- > 1) {
           this.queue.push(this.queue.shift()!);
       }
       return this.queue.shift()!;
    }

    top(): number {
        const temp: number = this.pop();
        this.queue.push(temp);
        return temp;
    }

    empty(): boolean {
       return !this.queue.length; 
    }
}

/**
 * Your MyStack object will be instantiated and called as such:
 * var obj = new MyStack()
 * obj.push(x)
 * var param_2 = obj.pop()
 * var param_3 = obj.top()
 * var param_4 = obj.empty()
 */