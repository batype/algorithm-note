/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
var topKFrequent = function(nums, k) {
    let map = new Map();
    for(const num of nums) {
        if(map.has(num)) {
            map.set(num, map.get(num) + 1);
        } else {
            map.set(num, 1);
        }
    }

    const head = new Heap((a, b) => a[1] - b[1]);

    for(const entry of map.entries()) {
        head.push(entry);

        if(head.size() > k) {
            head.pop();
        }
    }
    
    const res = [];

    for(let i = head.size() - 1; i >= 0; i--) {
        res[i] = head.pop()[0];
    }
    return res;
};

class Heap {
    queue;
    constructor(compareFn) {
       this.compareFn = compareFn;
       this.queue = [];
    }
    push(value) {
        this.queue.push(value);
        // 上浮
        let index = this.size() - 1;
        let parent = Math.floor((index - 1) / 2);

        while(index > 0 && this.compare(parent, index) > 0) {
            [this.queue[index], this.queue[parent]] = [this.queue[parent], this.queue[index]];
            index = parent;
            parent = Math.floor((index - 1) / 2);
        }
    }

    pop() {
        // 弹出堆顶元素
        const out = this.queue[0];
        // 弹出堆顶元素
        this.queue[0] = this.queue.pop();
        // 下沉
        let index = 0;
        let left = 1;
        let searchChild = this.compare(left, left + 1) > 0 ? left + 1 : left;
        while(searchChild !== undefined && this.compare(index, searchChild) > 0) {
            [this.queue[index], this.queue[searchChild]] = [this.queue[searchChild], this.queue[index]];
            index = searchChild;
            left = index * 2 + 1;
            searchChild = this.compare(left, left + 1) > 0 ? left + 1 : left;
        }
        return out;
    }

    size() {
        return this.queue.length;
    }

    compare(a, b) {
        if(this.queue[a] === undefined) return 1;
        if(this.queue[b] === undefined) return -1;

        return this.compareFn(this.queue[a], this.queue[b]);
    }
}

console.log(topKFrequent([1,1,1,2,2,3], 2));
console.log(topKFrequent([1], 1));
console.log(topKFrequent([1,2], 2));
console.log(topKFrequent([1,2,3,4,5,6,7,8,9,10], 3));