/**
 * @param {string} s
 * @return {string}
 */
var removeDuplicates = function(s) {
    let satck = [];
    for(let i = 0; i < s.length; i++) {
        if(satck.length && s[i] === satck[satck.length - 1]) {
            satck.pop();
        } else {
            satck.push(s[i]);
        }
    }
    return satck.join('');
};

console.log(removeDuplicates('abbaaca'));
console.log(removeDuplicates('abbbaca'));