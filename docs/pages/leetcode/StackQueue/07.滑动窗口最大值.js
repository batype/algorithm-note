class MyQueue {
    queue;
    constructor() {
        this.queue = [];
    }

    dequeue(val) {
        let max = this.getMax();
        if(max === val) {
            this.queue.shift();
        }
    }

    enqueue(val) {
        let back = this.queue[this.queue.length - 1];
        while(back !== undefined && back < val) {
            this.queue.pop();
            back = this.queue[this.queue.length - 1];
        }
        this.queue.push(val);
    }

    getMax() {
        return this.queue[0];
    }
}

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
var maxSlidingWindow = function(nums, k) {
    const queue = new MyQueue();
    
    let i = 0, j = 0;
    while(j < k) {
        queue.enqueue(nums[j++]);
    }
    const res = [queue.getMax()];
    while(j < nums.length) {
        queue.enqueue(nums[j]);
        queue.dequeue(nums[i]);
        res.push(queue.getMax());
        i++;
        j++;
    }
    return res;
};

console.log(maxSlidingWindow([1,3,-1,-3,5,3,6,7], 3));