function isValid(s) {
    let stack = [];
    if(s.length % 2 !== 0) return false;
    if(s.length === 0) return true;
    for(let i = 0; i < s.length; i++) {
        let str = s[i];
        switch(str) {
            case '(':
                stack.push(')');
                break;
            case '[':
                stack.push(']');
                break;
            case '{':
                stack.push('}');
                break;
            default:
                if(stack.length === 0 || stack.pop() !== str) {
                    return false;
                }
        }
        
    }
    return stack.length === 0;
};

console.log(isValid('()[]{}'));
console.log(isValid('()[{}'));
console.log(isValid('()){}'));
