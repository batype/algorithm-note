type BracketMap = {
    [key: string]: string;
}
function isValid(s: string): boolean {
    let stack: string[] = [];
    
    let bracketMap: BracketMap = {
        '(': ')',
        '[': ']',
        '{': '}'
    }

    for(let i of s) {
        if(bracketMap.hasOwnProperty(i)) {
            stack.push(bracketMap[i]);
        } else {
            if(stack.length === 0 || i !== stack.pop()) {
                return false;
            }
        }
    }

    return stack.length === 0;
};