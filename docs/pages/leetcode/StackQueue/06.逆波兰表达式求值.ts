function evalRPN(tokens: string[]): number {
    const stack: number[] = [];
    for (const token of tokens) {
        if(['+', '-', '*', '/'].includes(token)) {
            const a:number = stack.pop();
            const b:number = stack.pop(); 
            if (token === '+') {
                stack.push(b + a);
            } else if (token === '-') {
                stack.push(b - a);
            } else if (token === '*') {
                stack.push(b * a);
            } else if (token === '/') {
                stack.push(Math.trunc(b / a));
            }
        }
         else {
            stack.push(Number(token));
        }
    }
    return stack.pop();
};