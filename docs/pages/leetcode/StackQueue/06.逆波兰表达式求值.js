const operate = {
    '+': (a, b) => a + b,
    '-': (a, b) => a - b,
    '*': (a, b) => a * b,
    '/': (a, b) => Math.trunc(a / b)
}

/**
 * @param {string[]} tokens
 * @return {number}
 */
var evalRPN = function(tokens) {
    let helperStack = [];
    for(let i = 0; i < tokens.length; i++) {
        if(operate[tokens[i]]) {
            let b = Number(helperStack.pop());
            let a = Number(helperStack.pop());
            helperStack.push(operate[tokens[i]](a, b));
        } else {
            helperStack.push(Number(tokens[i]));
        }
    }
    return helperStack.pop();
};

console.log(evalRPN(["2","1","+","3","*"]))
console.log(evalRPN(["4","13","5","/","+"]))
console.log(evalRPN(["10","6","9","3","+","-11","*","/","*","17","+","5","+"]))