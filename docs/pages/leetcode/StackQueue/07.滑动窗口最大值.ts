class MyQueue {
    private queue: number[];
    constructor() {
        this.queue = [];
    }

    dequeue(val: number):void {
        let max: number | undefined = this.getMax();
        if(max !== undefined && max === val) {
            this.queue.shift();
        }
    }

    enqueue(val: number): void {
        let back: number | undefined = this.queue[this.queue.length - 1];
        while(back !== undefined && back < val) {
            this.queue.pop();
            back = this.queue[this.queue.length - 1];
        }
        this.queue.push(val);
    }

    getMax(): number  | undefined {
        return this.queue[0];
    }
}

function maxSlidingWindow(nums: number[], k: number): number[] {
    let i: number = 0,
        j: number = 0;
    const res: number[] = [];
    const queue: MyQueue = new MyQueue();
    while(j < nums.length) {
        queue.enqueue(nums[j]);
        if(j - i + 1 === k) {
            res.push(queue.getMax()!);
            queue.dequeue(nums[i]);
            i++;
        }
        j++;
    }
    return res;
};