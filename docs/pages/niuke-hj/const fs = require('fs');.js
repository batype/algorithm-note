const fs = require("fs");

const getFilesInFolder = (folderPath) => {
  const files = fs.readdirSync(folderPath);
  return files;
};

const writeFileWithContent = (filePath, content) => {
  fs.writeFileSync(filePath, content);
  console.log(`文件 ${filePath} 已成功生成并写入内容`);
};

// 示例用法
const folderPath = "./docs/pages/niuke-hj";
const files = getFilesInFolder(folderPath);
let arr = [];
for (let i = 0; i < files.length; i++) {
  let name = files[i];
  if (String(name).includes(".js")) {
    const filePath = `./docs/pages/niuke-hj/${String(name).replace(
      ".js",
      ""
    )}.md`;
    const content = `# ${String(name).replace(".js", "")}

@[code](./${name})`;
    writeFileWithContent(filePath, content);
  }
}

console.log(arr);
